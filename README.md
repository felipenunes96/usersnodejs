# Users Node.js



## Challenge

> Sua tarefa é fazer um aplicativo que carregue a saida da URL https://jsonplaceholder.typicode.com/users, que retorna uma lista de usuário em JSON.
Faça um programa que carregue a saída dessa URL e mostre os seguintes dados:

1. Os websites de todos os usuários
2. O Nome, email e a empresa em que trabalha (em ordem alfabética).
3. Mostrar todos os usuários que no endereço contem a palavra ```suite```
4. Salvar logs de todas interações no elasticsearch
5. EXTRA: Criar test unitário para validar os itens a cima.


## Installation
The project was made available in docker, for running the project go to devops directory and run:


```docker-compose
docker-compose up
```

To ensure that all containers are up, access the following routes


Elasticsearch

[http://localhost:9200/](http://localhost:9200/)

![image](images/elastic.PNG)


NodeJs Express Server

[http://localhost:8080/](http://localhost:8080/)

![image](images/node.PNG)

## Routes

There are a total of three routes to test


/user/website - Returns the sites of all users

[http://localhost:8080/user/websites](http://localhost:8080/user/websites)

```json
{
  "websites": [
    {
      "website": "hildegard.org"
    },
    {
      "website": "anastasia.net"
    },
    {
      "website": "ramiro.info"
    },
    {
      "website": "kale.biz"
    },
    {
      "website": "demarco.info"
    },
    {
      "website": "ola.org"
    },
    {
      "website": "elvis.io"
    },
    {
      "website": "jacynthe.com"
    },
    {
      "website": "conrad.com"
    },
    {
      "website": "ambrose.net"
    }
  ]
}
```


/user/usersSorted - Returns the name, email, company of all users sorted alphabetically

[http://localhost:8080/user/usersSorted](http://localhost:8080/user/usersSorted)

```json
{
    "users": [
        {
            "name": "Chelsey Dietrich",
            "email": "Lucio_Hettinger@annie.ca",
            "company": "Keebler LLC"
        },
        {
            "name": "Clementina DuBuque",
            "email": "Rey.Padberg@karina.biz",
            "company": "Hoeger LLC"
        },
        {
            "name": "Clementine Bauch",
            "email": "Nathan@yesenia.net",
            "company": "Romaguera-Jacobson"
        },
        {
            "name": "Ervin Howell",
            "email": "Shanna@melissa.tv",
            "company": "Deckow-Crist"
        },
        {
            "name": "Glenna Reichert",
            "email": "Chaim_McDermott@dana.io",
            "company": "Yost and Sons"
        },
        {
            "name": "Kurtis Weissnat",
            "email": "Telly.Hoeger@billy.biz",
            "company": "Johns Group"
        },
        {
            "name": "Leanne Graham",
            "email": "Sincere@april.biz",
            "company": "Romaguera-Crona"
        },
        {
            "name": "Mrs. Dennis Schulist",
            "email": "Karley_Dach@jasper.info",
            "company": "Considine-Lockman"
        },
        {
            "name": "Nicholas Runolfsdottir V",
            "email": "Sherwood@rosamond.me",
            "company": "Abernathy Group"
        },
        {
            "name": "Patricia Lebsack",
            "email": "Julianne.OConner@kory.org",
            "company": "Robel-Corkery"
        }
    ]
}
```

/user/address- Returns all users who have the word suite in the address

[http://localhost:8080/user/address](http://localhost:8080/user/address)

```json
{
  "users": [
    {
      "id": 2,
      "name": "Ervin Howell",
      "username": "Antonette",
      "email": "Shanna@melissa.tv",
      "address": {
        "street": "Victor Plains",
        "suite": "Suite 879",
        "city": "Wisokyburgh",
        "zipcode": "90566-7771",
        "geo": {
          "lat": "-43.9509",
          "lng": "-34.4618"
        }
      },
      "phone": "010-692-6593 x09125",
      "website": "anastasia.net",
      "company": {
        "name": "Deckow-Crist",
        "catchPhrase": "Proactive didactic contingency",
        "bs": "synergize scalable supply-chains"
      }
    },
    {
      "id": 3,
      "name": "Clementine Bauch",
      "username": "Samantha",
      "email": "Nathan@yesenia.net",
      "address": {
        "street": "Douglas Extension",
        "suite": "Suite 847",
        "city": "McKenziehaven",
        "zipcode": "59590-4157",
        "geo": {
          "lat": "-68.6102",
          "lng": "-47.0653"
        }
      },
      "phone": "1-463-123-4447",
      "website": "ramiro.info",
      "company": {
        "name": "Romaguera-Jacobson",
        "catchPhrase": "Face to face bifurcated interface",
        "bs": "e-enable strategic applications"
      }
    },
    {
      "id": 5,
      "name": "Chelsey Dietrich",
      "username": "Kamren",
      "email": "Lucio_Hettinger@annie.ca",
      "address": {
        "street": "Skiles Walks",
        "suite": "Suite 351",
        "city": "Roscoeview",
        "zipcode": "33263",
        "geo": {
          "lat": "-31.8129",
          "lng": "62.5342"
        }
      },
      "phone": "(254)954-1289",
      "website": "demarco.info",
      "company": {
        "name": "Keebler LLC",
        "catchPhrase": "User-centric fault-tolerant solution",
        "bs": "revolutionize end-to-end systems"
      }
    },
    {
      "id": 7,
      "name": "Kurtis Weissnat",
      "username": "Elwyn.Skiles",
      "email": "Telly.Hoeger@billy.biz",
      "address": {
        "street": "Rex Trail",
        "suite": "Suite 280",
        "city": "Howemouth",
        "zipcode": "58804-1099",
        "geo": {
          "lat": "24.8918",
          "lng": "21.8984"
        }
      },
      "phone": "210.067.6132",
      "website": "elvis.io",
      "company": {
        "name": "Johns Group",
        "catchPhrase": "Configurable multimedia task-force",
        "bs": "generate enterprise e-tailers"
      }
    },
    {
      "id": 8,
      "name": "Nicholas Runolfsdottir V",
      "username": "Maxime_Nienow",
      "email": "Sherwood@rosamond.me",
      "address": {
        "street": "Ellsworth Summit",
        "suite": "Suite 729",
        "city": "Aliyaview",
        "zipcode": "45169",
        "geo": {
          "lat": "-14.3990",
          "lng": "-120.7677"
        }
      },
      "phone": "586.493.6943 x140",
      "website": "jacynthe.com",
      "company": {
        "name": "Abernathy Group",
        "catchPhrase": "Implemented secondary concept",
        "bs": "e-enable extensible e-tailers"
      }
    },
    {
      "id": 9,
      "name": "Glenna Reichert",
      "username": "Delphine",
      "email": "Chaim_McDermott@dana.io",
      "address": {
        "street": "Dayna Park",
        "suite": "Suite 449",
        "city": "Bartholomebury",
        "zipcode": "76495-3109",
        "geo": {
          "lat": "24.6463",
          "lng": "-168.8889"
        }
      },
      "phone": "(775)976-6794 x41206",
      "website": "conrad.com",
      "company": {
        "name": "Yost and Sons",
        "catchPhrase": "Switchable contextually-based project",
        "bs": "aggregate real-time technologies"
      }
    },
    {
      "id": 10,
      "name": "Clementina DuBuque",
      "username": "Moriah.Stanton",
      "email": "Rey.Padberg@karina.biz",
      "address": {
        "street": "Kattie Turnpike",
        "suite": "Suite 198",
        "city": "Lebsackbury",
        "zipcode": "31428-2261",
        "geo": {
          "lat": "-38.2386",
          "lng": "57.2232"
        }
      },
      "phone": "024-648-3804",
      "website": "ambrose.net",
      "company": {
        "name": "Hoeger LLC",
        "catchPhrase": "Centralized empowering task-force",
        "bs": "target end-to-end models"
      }
    }
  ]
}
```


## Logging

All requests generate log in elasticsearch and can be viewed in

[http://localhost:9200/log/usersAPI/_search?](http://localhost:9200/log/usersAPI/_search?)


![image](images/log.PNG)


## Unit Tests

To execute the unit tests go to the project root folder and run
```bash
npm test
```
![image](images/test.PNG)



## Author
Luiz Felipe Araújo Nunes - luizfanunes@hotmail.com