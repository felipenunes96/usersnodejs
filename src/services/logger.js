const request = require('sync-request');
const config = require('../../configs/config.json');

const elasticHost = config.elastic.url
const elasticIndex = config.elastic.index
const elasticType = config.elastic.type

const log = (req, res, next) => {
    const oldWrite = res.write;
    const oldEnd = res.end;

    const chunks = [];

    res.write = (...restArgs) => {
        chunks.push(Buffer.from(restArgs[0]));
        oldWrite.apply(res, restArgs);
    };

    res.end = (...restArgs) => {
        if (restArgs[0]) {
            chunks.push(Buffer.from(restArgs[0]));
        }
        const body = Buffer.concat(chunks).toString('utf8');

        var logBody = {
            time: Date.now(),
            fromIP: req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress,
            method: req.method,
            originalUri: req.originalUrl,
            uri: req.url,
            requestData: req.body,
            responseData: body,
            referer: req.headers.referer || '',
            ua: req.headers['user-agent']
        };

        console.log(logBody);

        try {
            request('POST', `http://${elasticHost}/${elasticIndex}/${elasticType}`, {
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(logBody)
            });
        } catch (error) {
            console.log('Error logging into elasticsearch.');
        }

        oldEnd.apply(res, restArgs);
    };

    next();
}

module.exports = {log};