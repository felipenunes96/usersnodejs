﻿const usersInfo = require('../utils/usersInfo');
const sorter = require('../utils/sort');
const mapper = require('../utils/map');
const specification = require('../utils/specification');

const getWebSites = () => {
  return {
    'websites': mapper.usersToWebsites(usersInfo.retriveUsers())
  }
}

const getUsersInfoSorted = (columns) => {
  return {
    'users': sorter.sort(mapper.usersToNameEmailCompany(usersInfo.retriveUsers()), columns)
  }
}

const getWhereAddressLike = (search) => {
  return {
    'users': specification.filterUsersByAddress(usersInfo.retriveUsers(),search)
  }
}

module.exports = {
  getWebSites,
  getUsersInfoSorted,
  getWhereAddressLike
}