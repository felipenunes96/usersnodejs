const express = require('express');
const controller = require('../controllers/userController');

const router = express.Router();

router.get('/websites', controller.getWebSites);
router.get('/usersSorted', controller.getUsersInfoSorted);
router.get('/address', controller.getWhereAddressLike);

module.exports = router;