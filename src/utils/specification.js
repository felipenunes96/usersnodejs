﻿const filterUsersByAddress = (object, search) => {
    return object.filter(function (user) {
        if (user.address.suite.toLowerCase().includes(search))
            return true;
        return false;
    })
}

module.exports = {
    filterUsersByAddress
};