﻿const sort = (object, columns) => {

    sortedList = object;
    columns.forEach((item, i) => {
        sortedList = sortColumn(sortedList, columns, i);
    });
    return sortedList;
}

const sortColumn = (object, columns, index) => {
    if (index == 0) {
        return object.sort(function (a, b) {
            if (a[columns[index]] > b[columns[index]])
                return 1;

            if (a[columns[index]] == b[columns[index]])
                return 0;

            if (a[columns[index]] < b[columns[index]])
                return -1;
        });
    } else {
        return object.sort(function (a, b) {
            if (a[columns[index - 1]] == b[index - 1] && a[columns[index]] > b[columns[index]])
                return 1;

            if (a[columns] == b[columns] && a[columns[index]] == b[columns[index]])
                return 0;

            if (a[columns] == b[columns] && a[columns[index]] < b[columns[index]])
                return -1;
        });
    }
}

module.exports = {
    sort
};