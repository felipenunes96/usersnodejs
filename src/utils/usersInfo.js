﻿const request = require('sync-request');
const config = require('../../configs/config.json');

const url = config.data.users.url;

const retriveUsers = () => {
  var res = request('GET', url);
  return JSON.parse(res.getBody());
}

module.exports = {
  retriveUsers
};