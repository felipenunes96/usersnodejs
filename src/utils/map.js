﻿const usersToWebsites = (users) => {
    return users.map(function (user) {
        return { 'website': user.website };
    })
}

const usersToNameEmailCompany = (users) => {
    return users.map(function (user) {
        return { 'name': user.name, 'email': user.email, 'company': user.company.name }
    })
}

module.exports = {
    usersToWebsites,
    usersToNameEmailCompany
};