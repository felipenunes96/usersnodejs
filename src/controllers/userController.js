const userService = require('../services/userService');

exports.getWebSites = (req, res, next) => {
    res.status(200).send(userService.getWebSites());
};

exports.getUsersInfoSorted = (req, res, next) => {
    res.status(200).send(userService.getUsersInfoSorted(['name', 'email', 'company']));
};

exports.getWhereAddressLike = (req, res, next) => {
    res.status(200).send(userService.getWhereAddressLike('suite'));
};