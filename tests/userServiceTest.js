﻿const test = require('tape');
const userService = require('../src/services/userService');

test('Count Websites', (t) => {
    t.assert(userService.getWebSites().websites.length === 10, 'Loaded all websites');
    t.end();
})

test('Check if all websites have domain (.dot)', (t) => {
    t.assert(userService.getWebSites().websites.filter(function (website) {
        if (website.website.includes('.'))
            return false;
        return true;
    }).length === 0, 'All sites have domain');
    t.end()
})

test('Check are users is sorted', (t) => {
    var users = userService.getUsersInfoSorted(['name']).users;
    var validate = true;

    for (var i = 1; i <= users.length; i++) {
        if (users[i] - 1 < users[i]) {
            validate = false
            break;
        }
    }
    t.assert(validate, 'Users are sorted');
    t.end();
})


test('Verify that the user\'s address filter is working (CASE: Suite)', (t) => {
    var users = userService.getWhereAddressLike('suite').users;

    t.assert(users.filter(function (user) {
        if (user.address.suite.toLowerCase().includes('suite'))
            return true;
        return false;
    }).length == users.length, 'Filter done');
    t.end();
})

test('Verify that the user\'s address filter is working (CASE: APT)', (t) => {
    var users = userService.getWhereAddressLike('apt').users;

    t.assert(users.filter(function (user) {
        if (user.address.suite.toLowerCase().includes('apt'))
            return true;
        return false;
    }).length == users.length, 'Filter done');
    t.end();
})


